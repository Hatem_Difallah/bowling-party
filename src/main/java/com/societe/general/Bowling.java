package com.societe.general;

import java.util.List;

import org.apache.log4j.Logger;

import com.societe.general.models.Frame;
import com.societe.general.models.Line;
import com.societe.general.models.Roll;
import com.societe.general.parsers.FrameParser;
import com.societe.general.parsers.RollParser;

/**
 * This class consist to initialize game bowling
 * 
 * @author HATEM
 *
 */
public class Bowling {

	// LOGGER
	private static final Logger LOGGER = Logger.getLogger(Bowling.class.getName());

	private FrameParser frameParser;
	private RollParser rollParser;

	public Bowling() {
		this.frameParser = new FrameParser();
		this.rollParser = new RollParser();

	}

	/**
	 * resultatOfGame
	 * 
	 * @param line
	 * @return integer
	 */
	public int resultatOfGame(String line) throws Exception {

		LOGGER.debug("resultatOfGame(rolls=" + line + ") - [start]");

		List<Roll> rolls = rollParser.extractRollsList(line);
		List<Frame> frames = frameParser.extractFramesList(rolls);
		BowlingScore scorer = new BowlingScore(new Line(frames));
		int scoreParty = scorer.calculateScoreOfGame();
		
		LOGGER.debug("resultatOfGame(rolls=" + scoreParty + ") - [end]");

		return scoreParty;
	}

}
