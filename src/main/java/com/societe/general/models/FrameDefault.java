package com.societe.general.models;

import java.util.List;


import com.societe.general.utils.ApplicationConstants;
import com.societe.general.utils.MessageConstants;

import org.apache.log4j.Logger;

/**
 * This class is extends to frame and he is particular when the frame is default
 * in this class we have tow methods for validate and build frame
 * 
 * @author HATEM
 *
 */
public class FrameDefault extends Frame {

	// LOGGER
	private static final Logger LOGGER = Logger.getLogger(FrameDefault.class.getName());

	public FrameDefault() {
		super();
	}

	public FrameDefault(Roll firstRoll, Roll secondRoll, EFrameType type) {
		super(firstRoll, secondRoll, type);
	}

	/***
	 * This method consist to build frame
	 * 
	 * @param indexOfRoll
	 * @param rolls
	 * @return Frame
	 * 
	 */
	@Override
	public Frame buildFrame(int indexOfRoll, List<Roll> rolls) {

		LOGGER.debug("buildFrame(rolls=" + rolls + ",indexOfRoll=" + indexOfRoll + ") - [start]");

		FrameDefault frameDefault = null;
		
		// Get firstRoll & secondRoll
		Roll firstRoll = getRoll(indexOfRoll, rolls);
		Roll secondRoll = getRoll(indexOfRoll + 1, rolls);

		// check if this frame is valid
		if (isValidateFrame(indexOfRoll, rolls)) {
			frameDefault = new FrameDefault(firstRoll, secondRoll, EFrameType.DEFAUTL);
		}

		LOGGER.debug("buildFrame() - [end] - return : " + frameDefault);
		
		// return null if frame invalid and display Message
		LOGGER.warn(MessageConstants.ERROR_BUILD_FRAME_DEFAULT);
		return frameDefault;

	}

	/**
	 * This method consist to check if the frame is valid or not
	 * 
	 * @param indexOfRoll
	 * @param rolls
	 * @return boolean
	 */
	private boolean isValidateFrame(int indexOfRoll, List<Roll> rolls) {

		LOGGER.debug("isValidateFrame(rolls=" + rolls + ",indexOfRoll=" + indexOfRoll + ") - [start]");

		// build vauleFarme
		StringBuilder vauleFarme = new StringBuilder();
		Roll firstRoll = getRoll(indexOfRoll, rolls);
		Roll secondRoll = getRoll(indexOfRoll + 1, rolls);
		vauleFarme.append(firstRoll.getRollValue()).append(secondRoll.getRollValue());

		
		return firstRoll.getNumberOfPinsFall()
				+ secondRoll.getNumberOfPinsFall() < ApplicationConstants.SCORE_WHEN_KNOCK_DOWN_ALL;
	}

}
