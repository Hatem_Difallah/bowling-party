package com.societe.general.models;

import java.util.Collection;
import java.util.List;

import com.societe.general.utils.ApplicationConstants;

/**
 * This class is an presentation to all frame in bowling party , this Line
 * contains the list of frame
 * 
 * @author HATEM
 *
 */
public class Line {

	private List<Frame> frames;

	public Line(List<Frame> frames) {
		this.frames = frames;
	}

	public Collection<Frame> frames() {
		return frames.subList(ApplicationConstants.NUMBER_OF_FIRST_FRAME, ApplicationConstants.NUMBER_OF_LAST_FRAME);
	}

	/**
	 * This method consist to return next frame when needed for calculate score
	 * 
	 * @param frame
	 * @return Frame
	 */
	public Frame needNextFarme(Frame frame) {
		int numberOfTurn = frames.indexOf(frame);
		return frames.get(numberOfTurn + 1);
	}
}
