package com.societe.general.models;

import java.util.List;
import org.apache.log4j.Logger;


import com.societe.general.utils.ApplicationConstants;
import com.societe.general.utils.MessageConstants;

/**
 * This class is extends to frame and he is particular when the frame is spare
 * in this class we have three methods for validate and build frame and check
 * isSpare
 * 
 * @author HATEM
 *
 */
public class FrameSpare extends Frame {

	// LOGGER
	private static final Logger LOGGER = Logger.getLogger(FrameSpare.class.getName());

	public FrameSpare() {
		super();
	}

	public FrameSpare(Roll firstRoll, Roll secondRoll, EFrameType type) {
		super(firstRoll, secondRoll, type);
	}

	@Override
	public boolean isSpare() {
		return true;
	}

	/***
	 * This method consist to build frame
	 * 
	 * @param indexOfRoll
	 * @param rolls
	 * @return Frame
	 * 
	 */
	@Override
	public Frame buildFrame(int indexOfRoll, List<Roll> rolls) {

		LOGGER.debug("buildFrame(indexOfRoll=" + indexOfRoll + ",rolls=" + rolls + ") - [start]");

		// check if this frame is valid
		if (isValidateFrame(indexOfRoll, rolls)) {
			return new FrameSpare();
		}

		// return null if frame invalid and display Message
		LOGGER.warn(MessageConstants.ERROR_BUILD_FRAME_SPARE);
		return null;
	}

	/**
	 * This method consist to check if the frame is valid or not
	 * 
	 * @param indexOfRoll
	 * @param rolls
	 * @return boolean
	 */
	private boolean isValidateFrame(int indexOfRoll, List<Roll> rolls) {

		LOGGER.debug("isValidateFrame(rolls=" + rolls + ",indexOfRoll=" + indexOfRoll + ") - [start]");

		// build vauleFarme
		StringBuilder vauleFarme = new StringBuilder();
		vauleFarme.append(rolls.get(indexOfRoll).getRollValue());
		vauleFarme.append(rolls.get(indexOfRoll + 1).getRollValue());

		// match with REG_EXP
		return vauleFarme.toString().matches(ApplicationConstants.REG_EXP_FRAME_SPARE);
	}

}
