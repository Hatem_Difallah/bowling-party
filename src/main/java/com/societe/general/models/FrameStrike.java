package com.societe.general.models;

import java.util.List;

import org.apache.log4j.Logger;

import com.societe.general.utils.ApplicationConstants;
import com.societe.general.utils.MessageConstants;

/**
 * This class is extends to frame and he is particular when the frame is strike
 * in this class we have three methods for validate build frame and check
 * isStrike
 * 
 * @author HATEM
 *
 */
public class FrameStrike extends Frame {

	// LOGGER
	private static final Logger LOGGER = Logger.getLogger(FrameStrike.class.getName());

	public FrameStrike() {
		super();
	}

	public FrameStrike(Roll roll, EFrameType type) {
		super(roll, type);
	}

	@Override
	public boolean isStrike() {
		return true;
	}

	/***
	 * buildFrame
	 * 
	 * @param indexOfRoll
	 * @param rolls
	 * @return Frame
	 * 
	 */
	@Override
	public Frame buildFrame(int indexOfRoll, List<Roll> rolls) {

		LOGGER.debug("buildFrame(rolls=" + rolls + ",indexOfRoll=" + indexOfRoll + ") - [start]");

		Roll roll = getRoll(indexOfRoll, rolls);

		// check if this frame is valid
		if (isValidateFrame(indexOfRoll, rolls)) {
			return new FrameStrike(roll, EFrameType.STRIKE);
		}

		// return null if frame invalid and display Message
		LOGGER.warn(MessageConstants.ERROR_BUILD_FRAME_STRIKE);
		return null;

	}

	/**
	 * isValidateFrame
	 * 
	 * @param indexOfRoll
	 * @param rolls
	 * @return boolean
	 */
	private boolean isValidateFrame(int indexOfRoll, List<Roll> rolls) {

		LOGGER.debug("isValidateFrame(rolls=" + rolls + ",indexOfRoll=" + indexOfRoll + ") - [start]");

		// build vauleFarme
		StringBuilder vauleFarme = new StringBuilder();
		Roll firstRoll = rolls.get(indexOfRoll);
		vauleFarme.append(firstRoll.getRollValue());

		// match with REG_EXP
		return vauleFarme.toString().matches(ApplicationConstants.REG_EXP_FRAME_STRIKE);
	}

}
