package com.societe.general.models;

import java.util.List;

import org.apache.log4j.Logger;

import com.societe.general.utils.ApplicationConstants;
import com.societe.general.utils.MessageConstants;

/**
 * This class is extends to frame and he is particular when the frame is lost in
 * this class we have tow methods for validate and build frame
 * 
 * @author HATEM
 *
 */
public class FrameZero extends Frame {

	// LOGGER
	private static final Logger LOGGER = Logger.getLogger(FrameZero.class.getName());

	public FrameZero() {
		super();
	}

	public FrameZero(EFrameType type) {
		super(type);
	}

	/***
	 * This method consist to build frame
	 * 
	 * @param indexOfRoll
	 * @param rolls
	 * @return Frame
	 * 
	 */
	@Override
	public Frame buildFrame(int indexOfRoll, List<Roll> rolls) {

		LOGGER.debug("buildFrame(rolls=" + rolls + ",indexOfRoll=" + indexOfRoll + ") - [start]");

		// check if this frame is valid
		if (isValidateFrame(indexOfRoll, rolls)) {
			return new FrameZero(EFrameType.LOST);
		}

		// return null if frame invalid and display Message
		LOGGER.warn(MessageConstants.ERROR_BUILD_FRAME_ZERO);
		return null;
	}

	/**
	 * This method consist to check if the frame is valid or not
	 * 
	 * @param indexOfRoll
	 * @param rolls
	 * @return boolean
	 */
	private boolean isValidateFrame(int indexOfRoll, List<Roll> rolls) {

		LOGGER.debug("isValidateFrame(rolls=" + rolls + ",indexOfRoll=" + indexOfRoll + ") - [start]");

		// build vauleFarme
		StringBuilder checkFarme = new StringBuilder();
		checkFarme.append(rolls.get(indexOfRoll).getRollValue());
		checkFarme.append(rolls.get(indexOfRoll + 1).getRollValue());

		// match with REG_EXP
		return checkFarme.toString().matches(ApplicationConstants.REG_EXP_FRAME_ZERO);
	}

}
