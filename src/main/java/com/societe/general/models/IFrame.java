package com.societe.general.models;

import java.util.List;

public interface IFrame  {

	/**
	 * method implemented in class extends frame 
	 * 
	 * @param indexOfRoll
	 * @param rolls
	 * @return
	 */
	public Frame buildFrame(int indexOfRoll, List<Roll> rolls) ;

}
