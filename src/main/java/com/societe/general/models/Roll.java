package com.societe.general.models;

/**
 * This class consist to present the number of pins fall each roll
 * in this class we have tow attributes 
 * 
 * @author HATEM
 *
 */
public class Roll {

	private int numberOfPinsFall;
	private String rollValue;


	public Roll(int numberOfPinsFall, String rollValue) {
		this.numberOfPinsFall = numberOfPinsFall;
		this.rollValue = rollValue;
	}
	

	public Roll(int numberOfPinsFall) {
		super();
		this.numberOfPinsFall = numberOfPinsFall;
	}

	public int getNumberOfPinsFall() {
		return numberOfPinsFall;
	}

	public String getRollValue() {
		return rollValue;
	}

	@Override
	public String toString() {
		return "Roll [numberOfPinsFall=" + numberOfPinsFall + ", rollValue=" + rollValue + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + numberOfPinsFall;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Roll other = (Roll) obj;
		if (numberOfPinsFall != other.numberOfPinsFall)
			return false;
		return true;
	}

}
