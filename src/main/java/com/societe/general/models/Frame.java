package com.societe.general.models;

import java.util.List;

import com.societe.general.utils.ApplicationConstants;

/**
 * This class is an presentation to an frame in bowling , this frame contains
 * tow rolls and type of frame this class we have tow methods for validate and
 * build frame
 * 
 * @author HATEM
 *
 */
public class Frame implements IFrame{

	private Roll firstRoll;
	private Roll secondRoll;
	private EFrameType type;

	
	public Frame( EFrameType type) {
		this.type = type;
	}
	
	public Frame() {
		this(new Roll(ApplicationConstants.SCORE_WHEN_KNOCK_DOWN_ALL , "X"), EFrameType.STRIKE);
	}

	public Frame(Roll roll, EFrameType type) {
		this(roll, new Roll(ApplicationConstants.SCORE_WHEN_LOST ,"-"), EFrameType.LOST);
	}

	public Frame(Roll firstRoll, Roll secondRoll, EFrameType type) {
		this.firstRoll = firstRoll;
		this.secondRoll = secondRoll;
		this.type = type;
	}

	/***
	 *  This method consist to return the number of total pins fall
	 * 
	 * @return int
	 */
	public int getNumberOfPinsFall() {
		return firstRoll.getNumberOfPinsFall() + secondRoll.getNumberOfPinsFall();
	}

	/***
	 *  This method consist to return the number of pins fall in first roll
	 * 
	 * @return int
	 */
	public int numberOfPinsFallInFirstRoll() {
		return firstRoll.getNumberOfPinsFall();
	}

	/***
	 * This method consist to check if strike or not
	 * 
	 * @return boolean
	 * 
	 */
	public boolean isStrike() {
		return false;
	}

	/***
	 * This method consist to check if spare or not
	 * 
	 * @return boolean
	 * 
	 */
	public boolean isSpare() {
		return false;
	}
	
	/***
	 * This method consist to build frame
	 * 
	 * @param indexOfRoll
	 * @param rolls
	 * @return Frame
	 * 
	 */
	public Frame buildFrame(int indexOfRoll, List<Roll> rolls) {
		return null;
	}
	
	@Override
	public String toString() {
		return "Frame [firstRoll=" + firstRoll + ", secondRoll=" + secondRoll + ", type=" + type + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((firstRoll == null) ? 0 : firstRoll.hashCode());
		result = prime * result + ((secondRoll == null) ? 0 : secondRoll.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Frame other = (Frame) obj;
		if (firstRoll == null) {
			if (other.firstRoll != null)
				return false;
		} else if (!firstRoll.equals(other.firstRoll))
			return false;
		if (secondRoll == null) {
			if (other.secondRoll != null)
				return false;
		} else if (!secondRoll.equals(other.secondRoll))
			return false;
		if (type != other.type)
			return false;
		return true;
	}
	
	public Roll getRoll(int indexOfRoll, List<Roll> rolls) {
		return indexOfRoll < rolls.size() ? rolls.get(indexOfRoll)
				: new Roll(ApplicationConstants.SCORE_WHEN_LOST, "-");
	}

}
