package com.societe.general.parsers;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.societe.general.models.Roll;
import com.societe.general.utils.ApplicationConstants;
import com.societe.general.utils.MessageConstants;

/**
 * The class has role to parse an Roll
 * 
 * @author HATEM
 *
 */
public class RollParser {

	// LOGGER
	private static final Logger LOGGER = Logger.getLogger(RollParser.class.getName());

	/**
	 * This method consist to extract the list of roll
	 * 
	 * @param rolls
	 * @return List<Roll>
	 */
	public List<Roll> extractRollsList(String line) throws Exception {

		LOGGER.debug("extractRollsList(line=" + line + ") - [start]");

		try {
			List<Roll> result = new ArrayList<Roll>();
			for (int i = 0; i < line.length(); i++) {
				result.add(buildRoll(line, i));
			}
			LOGGER.debug("extractRollsList() - [end] - return : " + result);
			return result;
		} catch (NumberFormatException e) {
			LOGGER.error("extractRollsList() - [end] - exception : ", e);
			throw e;
		}

	}

	/**
	 * This method is for build Roll
	 * 
	 * @param rolls
	 * @param indexOfRoll
	 * @return Roll
	 */
	private Roll buildRoll(String rolls, int indexOfRoll) throws Exception {

		LOGGER.debug("buildRoll(rolls=" + rolls + ",indexOfRoll=" + indexOfRoll + ") - [start]");

		try {
			Integer score = changeSpecialCharacterToNumber(rolls, indexOfRoll);
			Roll roll = new Roll(score, rolls.charAt(indexOfRoll) + "");
			LOGGER.debug("buildRoll() - [end] - return : " + roll);
			return roll;
		} catch (NumberFormatException e) {
			LOGGER.error("buildRoll() - [end] - exception : ", e);
			throw e;
		}

	}

	/***
	 * this method consist to change special Character To Number
	 * 
	 * @param rolls
	 * @param indexOfRoll
	 * @return integer
	 */
	private Integer changeSpecialCharacterToNumber(String rolls, int indexOfRoll) {

		LOGGER.debug("changeSpecialCharacterToNumber(rolls=" + rolls + ",indexOfRoll=" + indexOfRoll + ") - [start]");

		try {

			char score = rolls.charAt(indexOfRoll);

			if (score == ApplicationConstants.SPECIAL_CHARACTER_WEHN_LOST) {
				return ApplicationConstants.SCORE_WHEN_LOST;
			}
			if (score == ApplicationConstants.SPECIAL_CHARACTER_WHEN_SPARE) {
				return ApplicationConstants.SCORE_WHEN_KNOCK_DOWN_ALL
						- changeSpecialCharacterToNumber(rolls, indexOfRoll - 1);
			}
			if (score == ApplicationConstants.SPECIAL_CHARACTER_WEHN_STRIKE) {
				return ApplicationConstants.SCORE_WHEN_KNOCK_DOWN_ALL;
			}

			LOGGER.debug("changeSpecialCharacterToNumber() - [end] - return : " + score);

			return Integer.parseInt(String.valueOf(score));

		} catch (NumberFormatException e) {

			LOGGER.debug("changeSpecialCharacterToNumber() - [end] - return : " + e);

			LOGGER.warn(MessageConstants.ERROR_BUILD_ROLL);
			throw e;
		}

	}

}
