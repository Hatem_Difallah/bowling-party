package com.societe.general.parsers;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.societe.general.models.Frame;
import com.societe.general.models.FrameDefault;
import com.societe.general.models.FrameSpare;
import com.societe.general.models.FrameStrike;
import com.societe.general.models.FrameZero;
import com.societe.general.models.Roll;
import com.societe.general.utils.ApplicationConstants;

/**
 * The class has role to parse an frame 
 * 
 * @author HATEM
 *
 */
public class FrameParser {

	// LOGGER
	private static final Logger LOGGER = Logger.getLogger(FrameParser.class.getName());
	
	/***
	 * This method consist to extract the list of frame
	 * 
	 * @param rolls
	 * @return List<Frame>
	 */
	public List<Frame> extractFramesList(List<Roll> rolls) {

		LOGGER.debug("extractFramesList(line=" + rolls + ") - [start]");

		// Initialize list frame
		List<Frame> frames = new ArrayList<Frame>();

		int numberOfRoll = 0;
		
		while (numberOfRoll < rolls.size()) {
			Frame frame = buildFrame(numberOfRoll, rolls);
			frames.add(frame);
			
			// increment according to type strike
			numberOfRoll += frame.isStrike() ? 1 : 2;
		}
		
		LOGGER.debug("extractFramesList() - [end] - return : " + frames);

		return frames;
	}

	/**
	 * This method is for build Frame
	 * 
	 * @param indexOfRoll
	 * @param rolls
	 * @return Frame
	 */
	public Frame buildFrame(int indexOfRoll, List<Roll> rolls) {

		LOGGER.debug("buildRoll(rolls=" + rolls + ",indexOfRoll=" + indexOfRoll + ") - [start]");

		// Get first Roll
		Roll firstRoll = getRoll(indexOfRoll, rolls);

		// check if Frame Strike
		if (firstRoll.getNumberOfPinsFall() == ApplicationConstants.SCORE_WHEN_KNOCK_DOWN_ALL) {

			FrameStrike frameStrike = new FrameStrike();
			return frameStrike.buildFrame(indexOfRoll, rolls);
		}

		// Get second Roll
		Roll secondRoll = getRoll(indexOfRoll + 1, rolls);

		// check if Frame Spare
		if (firstRoll.getNumberOfPinsFall()
				+ secondRoll.getNumberOfPinsFall() == ApplicationConstants.SCORE_WHEN_KNOCK_DOWN_ALL) {
			FrameSpare frameSpare = new FrameSpare();
			return frameSpare.buildFrame(indexOfRoll, rolls);
		}

		// check if Frame Zero
		if (firstRoll.getNumberOfPinsFall() + secondRoll.getNumberOfPinsFall() == ApplicationConstants.SCORE_WHEN_LOST) {
			FrameZero frameZero = new FrameZero();
			return frameZero.buildFrame(indexOfRoll, rolls);

		}
		
		// check if Frame Default
		if (firstRoll.getNumberOfPinsFall()
				+ secondRoll.getNumberOfPinsFall() < ApplicationConstants.SCORE_WHEN_KNOCK_DOWN_ALL) {

			FrameDefault frameDefault = new FrameDefault();
			return frameDefault.buildFrame(indexOfRoll, rolls);
		}


		return null;

	}

	/**
	 * Get roll 
	 * 
	 * @param indexOfRoll
	 * @param rolls
	 * @return Roll
	 */
	public Roll getRoll(int indexOfRoll, List<Roll> rolls) {
		return indexOfRoll < rolls.size() ? rolls.get(indexOfRoll) : new Roll(ApplicationConstants.SCORE_WHEN_LOST);
	}

}
