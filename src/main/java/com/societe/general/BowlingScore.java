package com.societe.general;

import org.apache.log4j.Logger;

import com.societe.general.models.Frame;
import com.societe.general.models.Line;

/**
 * This class consist to calculate score of game bowling
 * 
 * @author HATEM
 *
 */
public class BowlingScore {

	// LOGGER
	private static final Logger LOGGER = Logger.getLogger(Bowling.class.getName());

	private Line line;

	public BowlingScore() {
		super();
	}

	public BowlingScore(Line line) {
		this.line = line;
	}

	/**
	 * This method is to calculate Score Of Game
	 * 
	 * @return Integer
	 */
	public int calculateScoreOfGame() {

		LOGGER.debug("calculateScoreOfGame() - [start]");

		int result = 0;
		for (Frame frame : line.frames()) {
			result += calculateScoreOfFrame(frame);
		}

		LOGGER.debug("calculateScoreOfGame() - [end] - return : " + result);

		return result;
	}

	/**
	 * This method is to calculate Score Of Frame
	 * 
	 * @param frame
	 * @return Integer
	 */
	private int calculateScoreOfFrame(Frame frame) {
		return frame.getNumberOfPinsFall() + calculateBonus(frame);
	}

	/**
	 * This method is to calculate Bonus
	 * 
	 * @param frame
	 * @return Integer
	 */
	private int calculateBonus(Frame frame) {

		// check if Spare
		if (frame.isSpare()) {
			Frame nextFrame = line.needNextFarme(frame);
			return nextFrame.numberOfPinsFallInFirstRoll();
		}

		// check if Strike
		if (frame.isStrike()) {
			Frame nextFrame = line.needNextFarme(frame);
			if (nextFrame.isStrike()) {
				return nextFrame.numberOfPinsFallInFirstRoll()
						+ line.needNextFarme(nextFrame).numberOfPinsFallInFirstRoll();
			}
			return nextFrame.getNumberOfPinsFall();
		}

		return 0;
	}

}
