package com.societe.general.utils;

public final class MessageConstants {
	
	public static final String ERROR_BUILD_ROLL= "Erreur in check Roll";
	public static final String ERROR_BUILD_FRAME_SPARE  = "Erreur in check frame SPARE";
	public static final String ERROR_BUILD_FRAME_STRIKE = "Erreur in check frame STRIKE";
	public static final String ERROR_BUILD_FRAME_ZERO= "Erreur in check frame ZERO";
	public static final String ERROR_BUILD_FRAME_DEFAULT= "Erreur in check frame DEFAULT";

}
