package com.societe.general.utils;

/**
 * 
 * 
 * @author HATEM
 *
 */
public final class ApplicationConstants {


	// Regular Expression to validate frame
	public static final String REG_EXP_FRAME_SPARE   = "[1-9]{1}\\/{1}";
	public static final String REG_EXP_FRAME_STRIKE  = "X{1}" ;
	public static final String REG_EXP_FRAME_ZERO    = "\\-{2}" ;
	public static final String REG_EXP_FRAME_DEFAULT = "[0-9]{2}" ;
	public static final String REG_EXP_ROLL = "([1-9] | \\/ | \\X |\\-){1}";
	
	// number of first & last frame
	public static final int NUMBER_OF_LAST_FRAME= 10;
	public static final int NUMBER_OF_FIRST_FRAME= 0;
	
	// Score
	public static final int SCORE_WHEN_LOST= 0;
	public static final int SCORE_WHEN_KNOCK_DOWN_ALL= 10;
	
	// Special character 
	public static final char SPECIAL_CHARACTER_WEHN_LOST= '-';
	public static final char SPECIAL_CHARACTER_WEHN_STRIKE = 'X';
	public static final char SPECIAL_CHARACTER_WHEN_SPARE = '/';


}
