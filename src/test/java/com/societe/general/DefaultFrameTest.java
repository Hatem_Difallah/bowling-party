
package com.societe.general;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.societe.general.models.FrameDefault;
import com.societe.general.models.Roll;
import com.societe.general.models.Frame;

/***
 * 
 * 
 * @author HATEM
 *
 */
public class DefaultFrameTest {

	private static final String VALID_DEFAULT_FRAME = "36";
	private static final String INVALID_DEFAULT_FRAME = "X9";

	// TEST FrameDefault
	private FrameDefault defaultFrame;

	@Before
	public void setUp() {
		defaultFrame = new FrameDefault();
	}

	@Test
	public void should_Return_When_Valid_Input() {
		
		List<Roll> rolls = new ArrayList<Roll>();
		rolls.add(new Roll(3, VALID_DEFAULT_FRAME.substring(0, 1)));
		rolls.add(new Roll(6, VALID_DEFAULT_FRAME.substring(1, 2)));
		
		Frame result = defaultFrame.buildFrame(0 , rolls);

		assertTrue(result instanceof FrameDefault);
	}

	@Test
	public void should_Return_Null_When_Invalid_Input() {
		
		List<Roll> rolls = new ArrayList<Roll>();
		rolls.add(new Roll(7, INVALID_DEFAULT_FRAME.substring(0, 1)));
		rolls.add(new Roll(7, INVALID_DEFAULT_FRAME.substring(1, 2)));
		Frame result = defaultFrame.buildFrame(0 , rolls);

		assertNull(result);
	}

}
