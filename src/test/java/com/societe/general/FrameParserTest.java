
package com.societe.general;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.societe.general.models.Frame;
import com.societe.general.models.FrameStrike;
import com.societe.general.models.Roll;
import com.societe.general.parsers.FrameParser;

/***
 * 
 * 
 * @author HATEM
 *
 */
public class FrameParserTest {

	private FrameParser frameParser;

	@Before
	public void setUp() {
		frameParser = new FrameParser();
	}

	@Test
	public void extract_rolls_list_when_down_all() throws Exception {

		List<Frame> actual = frameParser.extractFramesList(initializeList_when_down_all());
		List<Frame> expected = initializeListExpected_when_down_all();
		
		assertArrayEquals(expected.toArray(), actual.toArray());
		assertEquals(actual.size(), 12);
	}

	private List<Roll> initializeList_when_down_all() {

		List<Roll> expected = new ArrayList<Roll>();
		for (int i = 0; i < 12; i++) {
			Roll roll = new Roll(10 , "X");
			expected.add(roll);
		}
		return expected;
	}

	private List<Frame> initializeListExpected_when_down_all() {

		List<Frame> expected = new ArrayList<Frame>();
		for (int i = 0; i < 12; i++) {
			FrameStrike frame = new FrameStrike();
			expected.add(frame);
		}
		return expected;
	}

}
