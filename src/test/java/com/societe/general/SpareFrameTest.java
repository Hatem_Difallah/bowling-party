
package com.societe.general;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.societe.general.models.Frame;
import com.societe.general.models.FrameSpare;
import com.societe.general.models.Roll;

/***
 * 
 * 
 * @author HATEM
 *
 */
public class SpareFrameTest {

	private static final String VALID_SPARE_FRAME = "1/";
	private static final String INVALID_SPARE_FRAME = "/2";

	private FrameSpare spare;

	@Before
	public void setUp() {
		spare = new FrameSpare();
	}

	@Test
	public void should_Return_When_Valid_Input() {
		List<Roll> rolls = new ArrayList<Roll>();
		rolls.add(new Roll(1, VALID_SPARE_FRAME.substring(0, 1)));
		rolls.add(new Roll(9, VALID_SPARE_FRAME.substring(1, 2)));

		Frame result = spare.buildFrame(0, rolls);

		assertTrue(result instanceof FrameSpare);
	}

	@Test
	public void should_Return_Null_When_Invalid_Input() {

		List<Roll> rolls = new ArrayList<Roll>();
		rolls.add(new Roll(1, INVALID_SPARE_FRAME.substring(0, 1)));
		rolls.add(new Roll(10, INVALID_SPARE_FRAME.substring(1, 2)));

		Frame result = spare.buildFrame(0, rolls);
		assertNull(result);
	}

}
