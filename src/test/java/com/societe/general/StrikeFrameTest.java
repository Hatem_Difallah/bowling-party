
package com.societe.general;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.societe.general.models.Frame;
import com.societe.general.models.FrameStrike;
import com.societe.general.models.Roll;

/***
 * 
 * 
 * @author HATEM
 *
 */
public class StrikeFrameTest {

	private static final String VALID_STRIKE_FRAME = "X";
	private static final String INVALID_STRIKE_FRAME = "2";

	private FrameStrike strike;

	@Before
	public void setUp() {
		strike = new FrameStrike();
	}

	@Test
	public void should_Return_When_Valid_Input() {

		List<Roll> rolls = new ArrayList<Roll>();
		rolls.add(new Roll(10, VALID_STRIKE_FRAME));

		Frame result = strike.buildFrame(0, rolls);

		assertTrue(result instanceof FrameStrike);
	}

	@Test
	public void should_Return_Null_When_Invalid_Input() {

		List<Roll> rolls = new ArrayList<Roll>();
		rolls.add(new Roll(2, INVALID_STRIKE_FRAME));

		Frame result = strike.buildFrame(0, rolls);

		assertNull(result);
	}

}
