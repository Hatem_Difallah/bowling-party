
package com.societe.general;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;

/***
 * 
 * 
 * @author HATEM
 *
 */
public class BowlingTest {

	// TEST Bowling
	private Bowling bowling;

	@Before
	public void setUp() {
		bowling = new Bowling();
	}

	@Test
	public void should_Return_300() throws Exception {
		assertThat(bowling.resultatOfGame("XXXXXXXXXXXX"), is(300));
	}

	@Test
	public void should_Return_150() throws Exception{
		assertThat(bowling.resultatOfGame("5/5/5/5/5/5/5/5/5/5/5"), is(200));
	}

	@Test
	public void should_Return_90()  throws Exception{
		assertThat(bowling.resultatOfGame("9-9-9-9-9-9-9-9-9-9-"), is(90));
	}

	
}
