
package com.societe.general;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.societe.general.models.Frame;
import com.societe.general.models.FrameZero;
import com.societe.general.models.Roll;

/***
 * 
 * 
 * @author HATEM
 *
 */
public class ZeroFrameTest {

	private static final String VALID_ZERO_FRAME = "--";
	private static final String INVALID_ZERO_FRAME = "9-";

	private FrameZero zero;

	@Before
	public void setUp() {
		zero = new FrameZero();
	}

	@Test
	public void should_Return_When_Valid_Input() {

		List<Roll> rolls = new ArrayList<Roll>();
		rolls.add(new Roll(0, VALID_ZERO_FRAME.substring(0, 1)));
		rolls.add(new Roll(0, VALID_ZERO_FRAME.substring(1, 2)));

		Frame result = zero.buildFrame(0, rolls);

		assertTrue(result instanceof FrameZero);
	}

	@Test
	public void should_Return_Null_When_Invalid_Input() {
		List<Roll> rolls = new ArrayList<Roll>();
		rolls.add(new Roll(0, INVALID_ZERO_FRAME.substring(0, 1)));
		rolls.add(new Roll(9, INVALID_ZERO_FRAME.substring(1, 2)));

		Frame result = zero.buildFrame(0, rolls);

		assertNull(result);
	}

}
