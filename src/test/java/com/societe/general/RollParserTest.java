
package com.societe.general;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.societe.general.models.Roll;
import com.societe.general.parsers.RollParser;
import com.societe.general.utils.MessageConstants;

/***
 * 
 * 
 * @author HATEM
 *
 */
public class RollParserTest {

	private RollParser rollParser;

	@Before
	public void setUp() {
		rollParser = new RollParser();
	}

	@Test
	public void extract_rolls_list_when_down_all() throws Exception {

		List<Roll> actual = rollParser.extractRollsList("XXXXXXXXXXXX");
		List<Roll> expected = initializeListExpected_when_down_all();
		assertArrayEquals(expected.toArray(), actual.toArray());
		assertEquals(actual.size(), 12);
	}

	@Test
	public void extract_rolls_list_when_failing_all() throws Exception {

		List<Roll> actual = rollParser.extractRollsList("--------------------");
		List<Roll> expected = initializeListExpected_when_failing_all();
		assertArrayEquals(expected.toArray(), actual.toArray());
		assertEquals(actual.size(), 20);
	}

	@Test
	public void extract_rolls_list_when_combined_normaly_case() throws Exception {

		List<Roll> actual = rollParser.extractRollsList("624418XX6/--223/XXX");
		List<Roll> expected = initializeListExpected_when_combined_normaly_case();
		assertArrayEquals(expected.toArray(), actual.toArray());
		assertEquals(actual.size(), 19);
	}

	@Rule
	public ExpectedException exceptionRule = ExpectedException.none();

	@Test(expected = NumberFormatException.class)
	public void extract_rolls_list_when_throw_exception() throws Exception {

		rollParser.extractRollsList("----------Z---------");
		exceptionRule.expect(NumberFormatException.class);
		exceptionRule.expectMessage(MessageConstants.ERROR_BUILD_ROLL);
	}

	private List<Roll> initializeListExpected_when_failing_all() {

		List<Roll> expected = new ArrayList<Roll>();
		for (int i = 0; i < 20; i++) {
			Roll roll = new Roll(0, "-");
			expected.add(roll);
		}
		return expected;
	}

	private List<Roll> initializeListExpected_when_down_all() {

		List<Roll> expected = new ArrayList<Roll>();
		for (int i = 0; i < 12; i++) {
			Roll roll = new Roll(10, "/");
			expected.add(roll);
		}
		return expected;
	}

	private List<Roll> initializeListExpected_when_combined_normaly_case() {

		List<Roll> expected = new ArrayList<Roll>();
		expected.add(new Roll(6, "6"));
		expected.add(new Roll(2, "2"));
		expected.add(new Roll(4, "4"));
		expected.add(new Roll(4, "4"));
		expected.add(new Roll(1, "1"));
		expected.add(new Roll(8, "8"));
		expected.add(new Roll(10, "10"));
		expected.add(new Roll(10, "10"));
		expected.add(new Roll(6, "6"));
		expected.add(new Roll(4, "4"));
		expected.add(new Roll(0, "0"));
		expected.add(new Roll(0, "0"));
		expected.add(new Roll(2, "2"));
		expected.add(new Roll(2, "2"));
		expected.add(new Roll(3, "3"));
		expected.add(new Roll(7, "7"));
		expected.add(new Roll(10, "10"));
		expected.add(new Roll(10, "10"));
		expected.add(new Roll(10, "10"));
		return expected;
	}

}
